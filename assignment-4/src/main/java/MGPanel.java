import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

public class MGPanel extends JPanel {
    private int correct = 0;
    private int pressed = 0;
    private JToggleButton toggleButton2;

    /**
     * Panel game yang berisi gambar2
     * Arraylist images untuk menyimpan seluruh gambar
     * Arraylist numbers untuk menyimpan nomor yang akan digunakan saat mengambil gambar
     */
    public MGPanel() {
        ArrayList<ImageIcon> images = new ArrayList<>();
        ArrayList<Integer> numbers1 = new ArrayList<>();
        ArrayList<Integer> numbers2 = new ArrayList<>();
        setLayout(new GridLayout(6,6));

        for (int i = 0; i < 18; i++) {
            numbers1.add(i);
            numbers2.add(i);
        }
        /**
         * mengacak nomor2 yang ada di arraylist numbers
         */
        Collections.shuffle(numbers1);
        Collections.shuffle(numbers2);

        for (int i = 1; i <= 18; i++) {
            images.add(new ImageIcon("../../../img/img" + i + ".png"));
        }

        /**
         * membuat toggle button dengan gambar default
         */
        for (int i = 0; i < 36; i++) {
            JToggleButton toggleButton = new JToggleButton(new ImageIcon("../../../img/default.png"));
            /**
             * mengeset gambar pada toggle button dari gambar2 yang ada di arraylist images
             */
            if (i < 18) {
                toggleButton.setSelectedIcon(images.get(numbers1.get(i)));
            } else {
                toggleButton.setSelectedIcon(images.get(numbers2.get(i-18)));
            }

            /**
             *
             */
            toggleButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pressed++;

                    /**
                     * ketika memencet toggle button 2 kali maka dihitung 1 kali percobaan
                     */
                    if (pressed == 2) {
                        MemoryGame.tryCounter();

                        /**
                         * ketika toggle button yang dipencet memiliki gambar yang sama dan juga memencet bukan toggle buton yang sama
                         * maka akan dihitung benar
                         */
                        if (toggleButton.getSelectedIcon().equals(toggleButton2.getSelectedIcon())
                                && !toggleButton.equals(toggleButton2)) {
                            pressed = 0;
                            correct++;
                            toggleButton.setVisible(false);
                            toggleButton2.setVisible(false);

                            /**
                             * jika perhitungan benar sudah mencapai 18 maka akan muncul message dialog dengan gambar dan text
                             */
                            if (correct == 18) {
                                ImageIcon icon = new ImageIcon(MGPanel.class.getResource("../../../img/win.png"));
                                JOptionPane.showMessageDialog(
                                        null,
                                        "Selamat Anda Menang !",
                                        "Win", JOptionPane.INFORMATION_MESSAGE,
                                        icon);
                            }

                            /**
                             * delay untuk menutup gambar
                             */
                            try {
                                Thread.sleep(100);
                            } catch (Exception ex) {
                                System.out.println(ex);
                            }
                        }
                        /**
                         * ketika toggle button dipencet dan tidak memiliki gambar yang sama
                         */
                        else {
                            toggleButton.setSelected(false);
                            toggleButton2.setSelected(false);
                            pressed = 0;

                            /**
                             * delay untuk menutup gambar
                             */
                            try {
                                Thread.sleep(100);
                            } catch (Exception ex) {
                                System.out.println(ex);
                            }
                        }
                    }

                    else {
                        toggleButton2 = toggleButton;
                    }
                }
            });
            /**
             * Membuat toggle button pada panel
             */
            add(toggleButton);
        }
    }

    /**
     * method reset saat diklik reset button sehingga menutup semua gambar dan juga mengubah correct menjadi 0 kembali
     */
    public void reset() {
        for (Component i : getComponents()) {
            JToggleButton button = (JToggleButton) i;
            i.setVisible(true);
            button.setSelected(false);
        }
    }
}
