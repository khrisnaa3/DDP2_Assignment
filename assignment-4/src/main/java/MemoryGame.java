import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MemoryGame extends JFrame{
    private static int tries = 0;
    private static JLabel countTried;
    
    public MemoryGame() {
        this.setTitle("Zoo Memory Game Dek Depe");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        MGPanel gamePanel = new MGPanel();

        /**
         * Buat panel yang berisi button dan juga text
         */
        JPanel button = new JPanel(new GridLayout(3,1));
        JButton resetButton = new JButton("           RESET            ");
        JButton exitButton = new JButton("           EXIT             ");
        countTried = new JLabel("Number of Tries : " + tries);

        /**
         * listener jika reset tombol dipencet
         */
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gamePanel.reset();
                tries = 0;
                countTried.setText("Number of Tries : " + tries);
            }
        });

        /**
         * listener jika tombol exit dipencet
         */
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        /**
         * memasukkan panel button dan game panel ke dalam frame
         */
        add(button);
        add(gamePanel);

        /**
         * memasukkan button dan text ke dalam panel button
         */
        button.add(resetButton);
        button.add(exitButton);
        button.add(countTried);

        /**
         * agar windows dapat dilihat
         */
        pack();
        setVisible(true);
    }

    /**
     * untung menghitung berapa kali percobaan dalam meng-klik
     */
    public static void tryCounter() {
        tries++;
        countTried.setText("Number of Tries : " + tries);
    }

    /**
     * menjalankan game nya
     */
    public static void main(String[] args) {
        new MemoryGame();
    }
}
