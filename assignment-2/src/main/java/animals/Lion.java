package main.java.animals;

import main.java.cages.OutdoorCage;

import java.util.Arrays;

public class Lion extends Animals {

    private static String[] behavior = {"1: See it hunting", "2: Brush the mane", "3: Disturb it"};
    private OutdoorCage cage;
    private static int counter;


    public Lion(String name, int length, OutdoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public OutdoorCage getCage() {
        return cage;
    }

    public static void getBehavior() {
        System.out.println(Arrays.toString(behavior).replace("[", "").replace("]", "").replace(",", ""));
    }

    public void hunt() {
        System.out.printf("Lion is hunting..\n%s makes a voice: err...!\n", getName());
    }

    public void brushMane() {
        System.out.printf("Clean the lion’s mane..\n%s makes a voice: Hauhhmm!\n", getName());
    }

    public void disturb() {
        System.out.println(getName() + " makes a voice: HAUHHMM!");
    }

    public static int getCounter() {
        return counter;
    }
}
