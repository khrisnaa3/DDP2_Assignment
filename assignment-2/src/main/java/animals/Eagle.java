package main.java.animals;

import main.java.cages.OutdoorCage;

import java.util.Arrays;

public class Eagle extends Animals {

    private static String[] behavior = {"1 : Order To Fly"};
    private OutdoorCage cage;
    private static int counter;

    public Eagle(String name, int length, OutdoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public OutdoorCage getCage() {
        return cage;
    }

    public static void getBehavior() {
        System.out.println(Arrays.toString(behavior).replace("[", "").replace("]", "").replace(",", ""));
    }

    public void orderToFly() {
        System.out.println(getName() + "makes a voice: kwaakk....");
        System.out.println("You Hurt!");
    }

    public static int getCounter() {
        return counter;
    }
}
