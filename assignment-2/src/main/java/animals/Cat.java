package main.java.animals;

import main.java.cages.IndoorCage;

import java.util.Arrays;
import java.util.Random;

public class Cat extends Animals {

    private static String[] behavior = {"1: Brush the fur", "2: Cuddle"};
    private String[] voices = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    private IndoorCage cage;
    private static int counter;

    public Cat(String name, int length, IndoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public static void getBehavior() {
        System.out.println(Arrays.toString(behavior).replace("[", "").replace("]", "").replace(",", ""));
    }

    public void brush() {
        System.out.println(getName() + "makes a voice: Nyaaan...");
    }

    public void cuddle() {
        int randomIndex = new Random().nextInt(voices.length);
        System.out.println(getName() + "makes a voice: " + voices[randomIndex]);
    }

    public static int getCounter() {
        return counter;
    }
}
