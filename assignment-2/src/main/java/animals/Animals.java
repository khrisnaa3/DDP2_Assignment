package main.java.animals;

public class Animals {
    private String name;
    private int length;

    public Animals(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }
}
