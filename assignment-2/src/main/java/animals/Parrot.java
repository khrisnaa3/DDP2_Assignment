package main.java.animals;

import main.java.cages.IndoorCage;

import java.util.Arrays;

public class Parrot extends Animals {

    private static String[] behavior = {"1: Order to fly", "2: Do conversation"};
    private IndoorCage cage;
    private static int counter;


    public Parrot(String name, int length, IndoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public static void getBehavior() {
        System.out.println(Arrays.toString(behavior).replace("[", "").replace("]", "").replace(",", ""));
    }

    public void orderToFly() {
        System.out.println(getName() + " makes a voice: FLYYYY.....");
    }

    public void doConversation(String speak) {
        System.out.println(getName() + " says: " + speak.toUpperCase());
    }

    public void noCommand() {
        System.out.println(getName() + "says: HM?");
    }

    public static int getCounter() {
        return counter;
    }
}
