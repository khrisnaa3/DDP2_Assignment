package main.java.animals;

import main.java.cages.IndoorCage;

import java.util.Arrays;

public class Hamster extends Animals {

    private static String[] behavior = {"1: See it gnawing", "2: Order to run in the hamster wheel"};
    private IndoorCage cage;
    private static int counter;

    public Hamster(String name, int length, IndoorCage cage) {
        super(name, length);
        this.cage = cage;
        counter++;
    }

    public IndoorCage getCage() {
        return cage;
    }

    public static void getBehavior() {
        System.out.println(Arrays.toString(behavior).replace("[", "").replace("]", "").replace(",", ""));
    }

    public void gnaw() {
        System.out.println(getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    public void hamsWheel() {
        System.out.println(getName() + " makes a voice: trrr.... trrr...");
    }

    public static int getCounter() {
        return counter;
    }
}
