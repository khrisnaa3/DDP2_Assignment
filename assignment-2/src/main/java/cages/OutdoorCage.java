package main.java.cages;

import main.java.animals.Animals;
import java.util.ArrayList;

public class OutdoorCage{
    private String tipe;

    public OutdoorCage(int length) {
        if (length <= 75) {
            this.tipe = "A";
        } else if (length >= 90) {
            this.tipe = "C";
        } else {
            this.tipe = "B";
        }
    }

    public String getTipe() {
        return tipe;
    }
}
