package main.java.cages;

public class IndoorCage {
    private String tipe;

    public IndoorCage(int length) {
        if (length <= 45) {
            this.tipe = "A";
        } else if (length >= 60) {
            this.tipe = "C";
        } else {
            this.tipe = "B";
        }
    }

    public String getTipe() {
        return tipe;
    }
}