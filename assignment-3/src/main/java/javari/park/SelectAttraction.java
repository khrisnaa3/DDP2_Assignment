package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;

/**
 * Author : Stefanus Khrisna
 *
 * Kelas ini sebagai implementasi dari Interface Class {Selected Attraction}
 * Kelas ini akan menyimpan Arraylist<Animal> performers yang dipilih pengunjung
 *
 */
public class SelectAttraction implements SelectedAttraction {
    String name, type;
    ArrayList<Animal> performers = new ArrayList<>();
;
    /**
     *
     * @param name : nama atraksi yang dipilih
     * @param type : tipe / jenis binatang yang dipilih
     */
    public SelectAttraction(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public ArrayList<Animal> getPerformers() {
        return performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
}
