package javari.park;

import java.util.ArrayList;
import java.util.List;

/**
 * Class ini mengimplementasikan dari Interface Class {Registration}
 * Class ini menyimpang Arraylist<SelectedAttraction> yang berisi objek" atraksi yang dipilih
 */

public class Register implements Registration {
    private static int counterID = 1, ID;
    private String visitorName;
    private ArrayList<SelectedAttraction> atraksi = new ArrayList<>();

    /**
     *
     * ID seseorang visitor akan terus bertambah sesuai urutan
     *
     */
    public Register() {
        this.ID = counterID;
        counterID++;
    }

    @Override
    public int getRegistrationId() {
        return ID;
    }

    @Override
    public String getVisitorName() {
        return visitorName;
    }

    @Override
    public String setVisitorName(String name) {
        visitorName = name;
        return visitorName;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return atraksi;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.atraksi.add(selected);
        return true;
    }
}
