package javari.animal;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Author : Stefanus Khrisna
 *
 * Class ini akan menyimpan Arraylist obj dari Mamals dan juga Mamals yang showable
 * Dalam class ini juga terjadi penyeleksian binatang layak show dan tidak
 *
 */
public class Mamals extends Animal {
    private String specialStatus;
    private static ArrayList<Animal> listObjMamals = new ArrayList<>();
    private static ArrayList<Animal> listMamalsShow = new ArrayList<>();

    /**
     *
     * {@code Mamals}sebagai child class dari abstract class {@code Animal}
     *
     * @param id : sebagai nomor id dari binatang mamals
     * @param type : sebagai tipe / jenis dari binatang, apakah lion, whale, dll.
     * @param name : nama dari binatang
     * @param gender : jenis kelaminnya
     * @param length : panjangnya
     * @param weight : beratnya
     * @param specialStatus : statusnya yang akan menjadi pertimbangan layak show atau tidak
     * @param condition : kondisinya yang juga menjadi pertimbangan layak show atau tidak
     */
    public Mamals(Integer id, String type, String name, Gender gender, double length, double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialStatus = specialStatus;
        specificCondition();
        if (isShowable()) {
            listMamalsShow.add(this);
        }
    }

    @Override
    protected boolean specificCondition() {
        if (specialStatus.equals("pregnant")) {
            return false;
        }
        if (getType().equals("Lion") && getGender().equals("female")) {
            return false;
        }
        return true;
    }

    public static void addListObjMamals(Animal objMamals) {
        Mamals.listObjMamals.add(objMamals);
    }

    public static ArrayList<Animal> getListObjMamals() {
        return listObjMamals;
    }

    public static ArrayList<Animal> getListMamalsShow() {
        return listMamalsShow;
    }
}
