package javari.animal;

import java.util.ArrayList;

/**
 * Author : Stefanus Khrisna
 *
 * Class ini akan menyimpan Arraylist obj dari Reptiles dan juga Reptiles yang showable
 * Dalam class ini juga terjadi penyeleksian binatang layak show dan tidak
 *
 */
public class Reptiles extends Animal {
    private String specialStatus;
    private static ArrayList<Animal> listObjReptiles = new ArrayList<>();
    private static ArrayList<Animal> listReptilesShow = new ArrayList<>();

    /**
     *
     * {@code Reptiles}sebagai child class dari abstract class {@code Animal}
     *
     * @param id : sebagai nomor id dari binatang reptiles
     * @param type : sebagai tipe / jenis dari binatang yaitu snake
     * @param name : nama dari binatang
     * @param gender : jenis kelaminnya
     * @param length : panjangnya
     * @param weight : beratnya
     * @param specialStatus : statusnya yang akan menjadi pertimbangan layak show atau tidak
     * @param condition : kondisinya yang juga menjadi pertimbangan layak show atau tidak
     */
    public Reptiles(Integer id, String type, String name, Gender gender, double length, double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialStatus = specialStatus;
        specificCondition();
        if (isShowable()) {
            listReptilesShow.add(this);
        }
    }

    @Override
    protected boolean specificCondition() {
        if (specialStatus.equals("wild")) {
            return false;
        }
        return true;
    }

    public static void addListObjReptiles(Animal objReptile) {
        Reptiles.listObjReptiles.add(objReptile);
    }

    public static ArrayList<Animal> getListObjReptiles() {
        return listObjReptiles;
    }

    public static ArrayList<Animal> getListReptilesShow() {
        return listReptilesShow;
    }
}
