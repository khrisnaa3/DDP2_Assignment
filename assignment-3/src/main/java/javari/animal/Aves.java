package javari.animal;

import java.util.ArrayList;

/**
 * Author : Stefanus Khrisna
 *
 * Class ini akan menyimpan Arraylist obj dari Aves dan juga Aves yang showable
 * Dalam class ini juga terjadi penyeleksian binatang layak show dan tidak
 *
 */

public class Aves extends Animal {
    private String specialStatus;
    private static ArrayList<Animal> listObjAves = new ArrayList<>();
    private static ArrayList<Animal> listAvesShow = new ArrayList<>();

    /**
     *
     * {@code Aves}sebagai child class dari abstract class {@code Animal}
     *
     * @param id : sebagai nomor id dari binatang aves
     * @param type : sebagai tipe / jenis dari binatang yaitu parrot / eagle
     * @param name : nama dari binatang
     * @param gender : jenis kelaminnya
     * @param length : panjangnya
     * @param weight : beratnya
     * @param specialStatus : statusnya yang akan menjadi pertimbangan layak show atau tidak
     * @param condition : kondisinya yang juga menjadi pertimbangan layak show atau tidak
     */
    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialStatus = specialStatus;
        specificCondition();
        if (isShowable()) {
            listAvesShow.add(this);
        }
    }

    @Override
    protected boolean specificCondition() {
        if (specialStatus.equals("laying eggs")) {
            return false;
        }
        return true;
    }

    public static void addListObjAves(Animal objAves) {
        Aves.listObjAves.add(objAves);
    }

    public static ArrayList<Animal> getListObjAves() {
        return listObjAves;
    }

    public static ArrayList<Animal> getListAvesShow() {
        return listAvesShow;
    }
}
