package javari;

import javari.animal.Animal;
import javari.park.Register;
import javari.park.SelectAttraction;
import javari.reader.Attractions;
import javari.reader.Records;
import javari.reader.Sections;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * Author : Stefanus Khrisna
 * Teman Diskusi = Kevin Raikhan, Feril Bagus, M. Adipashya
 *
 */

public class A3Festival {
    private static Scanner input = new Scanner(System.in);
    private static String path = "\\Users\\khrisna\\Documents\\UI\\Semester 2\\DDP 2\\DDP2_Assignment\\assignment-3\\data";
    private static Register pengunjung = new Register();

    public static void main(String[] args) throws IOException {
        System.out.print("Welcome to Javari Park Festival - Registration Service!\n\n"
                + "... Opening default section database from data.");
        while (true) {
            try {
                Attractions attractionReader = new Attractions(Paths.get(path, "animals_attractions.csv"));
                Sections sectionReader = new Sections(Paths.get(path, "animals_categories.csv"));
                Records recordReader = new Records(Paths.get(path, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");

                System.out.printf("Found %d valid sections and %d invalid sections\n", sectionReader.countValidRecords(), sectionReader.countInvalidRecords());
                System.out.printf("Found %d valid attractions and %d invalid attractions\n", attractionReader.countValidRecords(), attractionReader.countInvalidRecords());
                System.out.printf("Found %d valid animal categories and %d invalid animal categories\n", sectionReader.countValidCategory(), sectionReader.countInvalidCategory());
                System.out.printf("Found %d valid animal records and %d invalid animal records\n\n", recordReader.countValidRecords(), recordReader.countInvalidRecords());
                break;
            } catch (IOException e) {
                System.out.print(" ... File not found or incorrect file!\n");
                System.out.println("Please provide the source data path: ");
                path = input.nextLine();
            }
        }
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("Please answer the questions by typing the number.");
        System.out.println("Type # if you want to return to the previous menu\n");
        chooseSection();
    }

    /**
     * Memilih section untuk mengambil hewan yang akan atraksi
     */
    private static void chooseSection() {
        System.out.println("Javari Park has 3 sections:");
        int number = 1;
        for (String section : Sections.getSections()) {
            System.out.println(number + ". " + section);
            number++;
        }

        System.out.print("Please choose your preferred section (type the number): ");
        String choice = input.nextLine();
        try {
            if (choice.equals("#")) {
                chooseSection();
            } else if (Integer.parseInt(choice) <= Sections.getSections().size() && Integer.parseInt(choice) > 0) {
                chooseAnimal(Sections.getSections().get(Integer.parseInt(choice) - 1));
            }
        } catch (Exception e) {
            System.out.println(e);
            chooseSection();
        }
    }

    /**
     *
     * @param section : berisi section untuk mencari binatang apa saja yang ada didalamnya
     * Lalu mengecek apakah hewan tersebut memiliki sebuah atraksi atau tidak
     */
    private static void chooseAnimal(String section) {
        System.out.println("--" + section + "--");
        ArrayList<String> animals = new ArrayList<>();
        if (section.equalsIgnoreCase("Explore the Mammals")) {
            animals = Sections.getExploreTheMammals();
        } else if (section.equalsIgnoreCase("World of Aves")) {
            animals = Sections.getWorldOfAves();
        } else if (section.equalsIgnoreCase("Reptillian Kingdom")) {
            animals = Sections.getReptillianKingdom();
        }

        int number = 1;
        for (String animal : animals) {
            System.out.println(number + ". " + animal);
            number++;
        }

        System.out.print("Please choose your preferred animal (type the number): ");
        String choice = input.nextLine();
        try {
            if (choice.equals("#")) {
                chooseSection();
            } else if (Integer.parseInt(choice) <= animals.size() && Integer.parseInt(choice) > 0) {
                boolean bisaAtraksi = false;
                String animalChoice = animals.get(Integer.parseInt(choice) - 1);
                for (Animal hewan : Records.getListAnimals()) {
                    if (hewan.getType().equalsIgnoreCase(animalChoice) && hewan.isShowable()) {
                        bisaAtraksi = true;
                        break;
                    }
                }
                if (bisaAtraksi) {
                    chooseAttraction(section, animalChoice);
                } else {
                    System.out.println("Unfortunately, no " + animalChoice
                            + " can perform any attraction, please choose other animals");
                    chooseAnimal(section);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            chooseAnimal(section);
        }
    }

    /**
     *
     * @param section : Section yang dipilih
     * @param animal : Jenis binatang yang dipilih
     *
     * dalam Method ini, akan ada penyeleksian apakah dalam suatu atraksi terdapat hewan tersebut atau tidak
     */
    private static void chooseAttraction(String section, String animal) {
        System.out.println("Attraction(s) by " + animal);
        ArrayList<String> listAtraction = new ArrayList<>();
        if (Attractions.getCircleOfFire().contains(animal)) {
            listAtraction.add("Circles Of Fires");
        }
        if (Attractions.getDancingAnimals().contains(animal)) {
            listAtraction.add("Dancing Animals");
        }
        if (Attractions.getCountingMasters().contains(animal)) {
            listAtraction.add("Counting Masters");
        }
        if (Attractions.getPassionateCoders().contains(animal)) {
            listAtraction.add("Passionate Coders");
        }

        int number = 1;
        for (String atraksi : listAtraction) {
            System.out.println(number + ". " + atraksi);
            number++;
        }

        System.out.println("Please choose your preferred attractions (type the number): ");
        String choiceString = input.nextLine();
        try {
            if (choiceString.equals("#")) {
                chooseAnimal(section);
            } else if (Integer.parseInt(choiceString) <= listAtraction.size() && Integer.parseInt(choiceString) > 0) {
                int choiceInt = Integer.parseInt(choiceString);
                SelectAttraction attraction = new SelectAttraction(listAtraction.get(choiceInt - 1), animal);
                for (Animal hewan : Records.getListAnimals()) {
                    if (hewan.getType().equalsIgnoreCase(animal)) {
                        attraction.addPerformer(hewan);
                    }
                }
                inputNama(attraction);
            }
        } catch (Exception e) {
            System.out.println(e);
            chooseAttraction(section, animal);
        }
    }

    /**
     *
     * @param attraction : berisi objek atraksi yang dipilih oleh pengunjung
     * Menginput nama pengunjung
     *
     */
    private static void inputNama(SelectAttraction attraction) {
        System.out.println("Wow, one more step!");
        System.out.print("Please let us know your name: ");
        String name = input.nextLine();
        pengunjung.setVisitorName(name);
        check(pengunjung, attraction);
    }

    /**
     *
     * @param pengunjung : berisi objek pengunjung
     * @param attraction : berisi objek atraksi yang dipilih
     *
     * Jika pengunjung membenarkan data, maka akan diteruskan ke method chooseAnother
     * Jika pengujung tidak membenarkan data, maka akan kembali ke menu awal
     * Jika tidak keduanya, akan terjadi input error
     *
     */
    private static void check(Register pengunjung, SelectAttraction attraction) {
        System.out.println("Yeay, last check!");
        System.out.println("Here is your data, and the attraction you chose:");
        System.out.println("Name: " + pengunjung.getVisitorName());
        System.out.println("Attraction: " + attraction.getName() + " -> " + attraction.getType());
        StringBuilder performer = new StringBuilder();
        for (Animal hewan : attraction.getPerformers()) {
            performer.append(hewan.getName()).append(", ");
        }
        System.out.println("With: " + performer.substring(0, performer.length() - 2));

        System.out.print("Is the data correct? (Y/N): ");
        switch (input.nextLine()) {
            case "Y":
                pengunjung.addSelectedAttraction(attraction);
                chooseAnother();
                break;
            case "N":
                System.out.println("Resetting");
                chooseSection();
                break;
            default:
                System.out.println("Input error");
                check(pengunjung, attraction);
                break;
        }
    }

    /**
     * Jika pengunjung ingin menambah pendaftaran lagi, maka akan kembali ke menu awal
     * Jika tidak ingin menambah maka akan langsung di cetak
     */
    private static void chooseAnother() {
        System.out.println("Thank you for your interest. ");
        System.out.println("Would you like to register to other attractions? (Y/N): ");
        switch (input.nextLine()) {
            case "Y":
                chooseSection();
                break;
            case "N":
                printJson();
                break;
            default:
                System.out.println("Input error");
                chooseAnother();
                break;
        }
    }

    /**
     * Method ini untuk mencetak file json
     */
    private static void printJson() {
        try {
            RegistrationWriter.writeJson(pengunjung, Paths.get(path));
        } catch (IOException e) {
            System.out.println("\n... Destination folder not found ...\n");
            System.out.println("Please provide destination folder path: ");
            path = input.nextLine();
            printJson();
        }
    }
}