package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * Class ini sebagai child class dari Abstract Class {Csv Reader}
 * Class ini akan menyeleksi ke-valid-an dalam setiap section dan juga category
 *
 */
public class Sections extends CsvReader {
    private int validSection, invalidSection, validCategory, invalidCategory, aves, avesInvalid, mammalInvalid,
            reptileInvalid, exploreTheMammalsInvalid, worldOfAvesInvalid, reptillianKingdomInvalid;
    private static ArrayList<String> sections = new ArrayList<>(Arrays.asList("Explore The Mammals", "World Of Aves", "Reptillian Kingdom"));
    private static ArrayList<String> exploreTheMammals = new ArrayList<>(Arrays.asList("Hamster", "Whale", "Cat", "Lion"));
    private static ArrayList<String> worldOfAves = new ArrayList<>(Arrays.asList("Parrot", "Eagle"));
    private static ArrayList<String> reptillianKingdom = new ArrayList<>(Arrays.asList("Snake"));


    /**
     *
     * @param file : csv file yang akan dibaca
     * @throws IOException : jika error akan dilempar ke IOException
     *
     */
    public Sections(Path file) throws IOException {
        super(file);

        for (String i : lines) {

            String type = i.split(COMMA)[0];
            String category = i.split(COMMA)[1];
            String section = i.split(COMMA)[2];

            sectionChecker(type, section);
        }
    }

    /**
     *
     * @param type : tipe dari binatang
     * @param section : section dari binatang
     *
     * @code sectionChecker akan mengecek ke-valid-an berdasarkan ArrayList section yang telah ditentukan
     * sekaligus mengecek ke-valid-an category-nya
     *
     */
    public void sectionChecker(String type, String section) {
        switch (section) {
            case "Explore the Mamals" :
                for (String i : exploreTheMammals) {
                    if (!i.equalsIgnoreCase(type)) {
                        exploreTheMammalsInvalid = 1;
                        mammalInvalid = 1;
                    }
                }
                break;
            case "World of Aves" :
                for (String i :worldOfAves) {
                    if (!i.equalsIgnoreCase(type)) {
                        worldOfAvesInvalid = 1;
                        avesInvalid = 1;
                    }
                }
                break;
            case "Reptillian Kingdom" :
                for (String i : reptillianKingdom) {
                    if (!i.equalsIgnoreCase(type)) {
                        reptillianKingdomInvalid = 1;
                        reptileInvalid = 1;
                    }
                }
                break;
        }
    }

    @Override
    public long countValidRecords() {
        countInvalidRecords();
        return validSection = 3 - invalidSection;
    }

    @Override
    public long countInvalidRecords() {
        return invalidSection = worldOfAvesInvalid + exploreTheMammalsInvalid + reptillianKingdomInvalid;
    }

    public long countValidCategory() {
        countInvalidCategory();
        return validCategory = 3 - invalidCategory;
    }

    public long countInvalidCategory() {
        return invalidCategory = mammalInvalid + avesInvalid + reptileInvalid;
    }

    public static ArrayList<String> getExploreTheMammals() {
        return exploreTheMammals;
    }

    public static ArrayList<String> getWorldOfAves() {
        return worldOfAves;
    }

    public static ArrayList<String> getReptillianKingdom() {
        return reptillianKingdom;
    }

    public static ArrayList<String> getSections() {
        return sections;
    }
}
