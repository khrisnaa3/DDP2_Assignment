package javari.reader;

import javari.animal.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 *
 * CLass ini sebagai child class dari CsvReader
 * Class ini akan membuat objek-objek mamals, aves, reptiles dari file csv yang dibaca
 *
 */

public class Records extends CsvReader {
    private static ArrayList<Animal> listAnimals = new ArrayList<>();

    /**
     *
     * @param file : csv file yang akan dibaca
     * @throws IOException : terjadi error akan dilempar ke IOException
     */
    public Records(Path file) throws IOException {
        super(file);

        String id, animal, name, gender, length, weight, specialStatus, condition, type;
        for (String i : lines) {
            String[] index = i.split(COMMA);

            id = index[0];
            animal = index[1];
            name = index[2];
            gender = index[3];
            length  = index[4];
            weight = index[5];
            specialStatus = index[6];
            condition = index[7];

            if (animal.matches("Hamster|Whale|Cat|Lion")) {
                Mamals.addListObjMamals(new Mamals(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
                listAnimals.add(new Mamals(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
            }
            else if (animal.matches("Parrot|Eagle")) {
                Aves.addListObjAves(new Aves(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
                listAnimals.add(new Aves(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
            }
            else if (animal.matches("Snake")) {
                Reptiles.addListObjReptiles(new Reptiles(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
                listAnimals.add(new Reptiles(Integer.parseInt(id), animal, name, Gender.parseGender(gender), Double.parseDouble(length), Double.parseDouble(weight), specialStatus, Condition.parseCondition(condition)));
            }
        }
    }

    @Override
    public long countValidRecords() {
        return 0;
    }

    @Override
    public long countInvalidRecords() {
        return 0;
    }

    public static ArrayList<Animal> getListAnimals() {
        return listAnimals;
    }
}
