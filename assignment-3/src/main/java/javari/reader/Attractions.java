package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class ini sebagai child class dari Abstract Class {CsvReader}
 * Class ini akan mengecek ke-valid-an dari setiap atraksi
 */

public class Attractions extends CsvReader {
    private int valid, invalid, circleOfFiresInvalid, dancingAnimalsInvalid, countingMastersInvalid, passionateCodersInvalid;

    private static ArrayList<String> circleOfFire = new ArrayList<>(Arrays.asList("Lion", "Whale", "Eagle"));
    private static ArrayList<String> dancingAnimals = new ArrayList<>(Arrays.asList("Parrot", "Cat", "Snake", "Hamster"));
    private static ArrayList<String> countingMasters = new ArrayList<>(Arrays.asList("Hamster", "Whale", "Parrot"));
    private static ArrayList<String> passionateCoders = new ArrayList<>(Arrays.asList("Snake", "Cat", "Hamster"));

    /**
     *
     * @param file : berisi file csv yang akan dibaca
     * @throws IOException : jika terjadi suatu error akan dilempar ke IOException
     */

    public Attractions(Path file) throws IOException {
        super(file);
        attractionChecker();
    }

    /**
     * Cek ke-valid-an dari ArrayList yang telah ditentukan dari setiap atraksi
     */

    public void attractionChecker() {
        for (String i : lines) {
            String type = i.split(COMMA)[0];
            String attraction = i.split(COMMA)[1];
            switch (attraction) {
                case "Circle of Fires":
                    for (String u : circleOfFire) {
                        if (!u.equalsIgnoreCase(type)) {
                            circleOfFiresInvalid = 1;
                        }
                    }
                    break;
                case "Dancing Animals":
                    for (String u : dancingAnimals) {
                        if (!u.equalsIgnoreCase(type)) {
                            dancingAnimalsInvalid = 1;
                        }
                    }
                    break;
                case "Counting Masters":
                    for (String u : countingMasters) {
                        if (!u.equalsIgnoreCase(type)) {
                            countingMastersInvalid = 1;
                        }
                    }
                    break;
                case "Passionate Coders":
                    for (String u : passionateCoders) {
                        if (!u.equalsIgnoreCase(type)) {
                            passionateCodersInvalid = 1;
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public long countInvalidRecords() {
        return invalid = circleOfFiresInvalid + dancingAnimalsInvalid + countingMastersInvalid + passionateCodersInvalid;
    }

    @Override
    public long countValidRecords() {
        countInvalidRecords();
        return valid = 4 - invalid;
    }

    public static ArrayList<String> getCircleOfFire() {
        return circleOfFire;
    }

    public static ArrayList<String> getDancingAnimals() {
        return dancingAnimals;
    }

    public static ArrayList<String> getCountingMasters() {
        return countingMasters;
    }

    public static ArrayList<String> getPassionateCoders() {
        return passionateCoders;
    }
}
